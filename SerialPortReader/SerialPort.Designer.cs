﻿namespace SerialPortReader
{
    partial class SerialPortRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SerialPortRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnReadSerialPort = this.Factory.CreateRibbonButton();
            this.cbPort1 = this.Factory.CreateRibbonDropDown();
            this.btnClosePort = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "Beans";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnReadSerialPort);
            this.group1.Items.Add(this.cbPort1);
            this.group1.Items.Add(this.btnClosePort);
            this.group1.Label = "Serial Port";
            this.group1.Name = "group1";
            // 
            // btnReadSerialPort
            // 
            this.btnReadSerialPort.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnReadSerialPort.Enabled = false;
            this.btnReadSerialPort.Image = global::SerialPortReader.Properties.Resources.icon_serial_monitor;
            this.btnReadSerialPort.Label = "Read Port";
            this.btnReadSerialPort.Name = "btnReadSerialPort";
            this.btnReadSerialPort.ShowImage = true;
            this.btnReadSerialPort.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnReadSerialPort_Click);
            // 
            // cbPort1
            // 
            this.cbPort1.Label = "Port";
            this.cbPort1.Name = "cbPort1";
            this.cbPort1.SelectionChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.cbPorts_SelectionChanged);
            // 
            // btnClosePort
            // 
            this.btnClosePort.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnClosePort.Enabled = false;
            this.btnClosePort.Image = global::SerialPortReader.Properties.Resources.icon_vspman;
            this.btnClosePort.Label = "Close Port";
            this.btnClosePort.Name = "btnClosePort";
            this.btnClosePort.ShowImage = true;
            this.btnClosePort.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnClosePort_Click);
            // 
            // SerialPortRibbon
            // 
            this.Name = "SerialPortRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.SerialPort_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnReadSerialPort;
        internal Microsoft.Office.Tools.Ribbon.RibbonDropDown cbPort1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnClosePort;
    }

    partial class ThisRibbonCollection
    {
        internal SerialPortRibbon SerialPort
        {
            get { return this.GetRibbon<SerialPortRibbon>(); }
        }
    }
}
