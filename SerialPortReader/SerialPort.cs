﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Office.Tools.Ribbon;
using Microsoft.Office.Core;

namespace SerialPortReader
{
    public partial class SerialPortRibbon
    {

        private SerialPort serialPort1;

        private void SerialPort_Load(object sender, RibbonUIEventArgs e)
        {
            RibbonDropDownItem item = this.Factory.CreateRibbonDropDownItem();
            item.Label = "";
            cbPort1.Items.Add(item);
            item = this.Factory.CreateRibbonDropDownItem();
            
            foreach (String p in GetAllPorts())
            {
                item = this.Factory.CreateRibbonDropDownItem();
                item.Label = p;
                cbPort1.Items.Add(item);
                item = this.Factory.CreateRibbonDropDownItem();
                item.Label = p;
            }

        }
        private bool ValidatePortSelected(String portName, String idcombo)
        { 
            foreach(RibbonGroup group in this.Tabs[0].Groups )
            {
               foreach (RibbonControl ctrl in group.Items)
               {
                   if (ctrl is RibbonDropDown)
                   {
                       if (ctrl.Id.Contains("cbPort") && !ctrl.Id.Equals(idcombo) && ((RibbonDropDown)ctrl).SelectedItem.Label.Equals(portName))
                       {
                           return true;  
                       }
                   }
               }
            }

            return false;
        }

        private void btnReadSerialPort_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                    String port = this.cbPort1.SelectedItem.Label;
                MessageBox.Show(port);
                
                if (!String.IsNullOrEmpty(port))
                {                    
                    serialPort1 = new SerialPort();
                    serialPort1.PortName = port;
                    //Provide the name of port to which device is connected
                    //default values of hardware[check with device specification document]
                    serialPort1.BaudRate = 9600;
                    serialPort1.Parity = Parity.None;
                    serialPort1.StopBits = StopBits.One;
                    serialPort1.DataBits = 8;
                    serialPort1.Handshake = Handshake.None;
                    serialPort1.Encoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                    if (!serialPort1.IsOpen){
                        serialPort1.Open(); //opens the port
                        MessageBox.Show(String.Format("Port {0} opened", port));
                        /*serialPort1.DiscardInBuffer();
                        Thread.Sleep(100); // Always sleep before reading. Just a good measure to ensure the device has written to the buffer.
                        serialPort1.DiscardInBuffer();*/
                        serialPort1.ReadTimeout = 1000;
                        //serialPort1.Encoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                        if (serialPort1.IsOpen)
                        {
                            this.btnReadSerialPort.Enabled = false;
                            this.btnClosePort.Enabled = true;
                            serialPort1.DataReceived += SerialPort1_DataReceived; ;
                        }
                        else {
                            MessageBox.Show("Port is busy");
                        }
                    }
                }
                else
                    MessageBox.Show("Port don't especified");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);     
            }
     
        }

        private void SerialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            try
            {
                #region btnReadSerialPort_Click
                /*String value = serialPort1.ReadExisting();
                //String value = serialPort1.ReadBufferSize.ToString();
                System.Diagnostics.Debug.WriteLine(String.Format(" value [{0}] ", value));*/

                Thread.Sleep(100);

                //byte[] ReceiveBuffer = new byte[8];

                //int offset = 0;

                //int toRead = serialPort1.BytesToRead;

                //serialPort1.Read(ReceiveBuffer, offset, toRead);       

                //offset += toRead;
                //string s = System.Text.ASCIIEncoding.ASCII.GetString(ReceiveBuffer);

                //string s = System.Text.Encoding.UTF7.GetString(ReceiveBuffer);

                //String s = System.Text.Encoding.Default.GetString(ReceiveBuffer);

                //String ascii = System.Text.Encoding.Default.GetString(ReceiveBuffer);

                //byte[] bHex = Encoding.ASCII.GetBytes(barCodeString);

                // check it worked

                //byte[] hex = System.Text.Encoding.Default.GetBytes(ascii);

                //StringBuilder str = new StringBuilder();

                /*int bytePos = 0;

                foreach (byte b in bHex)
                {                
                   System.Diagnostics.Debug.Write(String.Format("{0:x2} ", b));

                    str.Append(String.Format("[{0:x2} pos {1}] ", b, bytePos));

                    bytePos++;
                }*/
                /* List<String> list = new List<string>();

                 list.Add(String.Format("{0:x2} ", hex[2]));

                 list.Add(String.Format("{0:x2} ", hex[3]));

                 list.Add(String.Format("{0:x2} ", hex[4]));

                 list.Reverse();

                 foreach (String s in list)
                     str.Append(s.Trim());*/

                //serialPort1.DiscardInBuffer();
                serialPort1.DiscardNull = true;
                int bytesToRead = serialPort1.BytesToRead;
                //serialPort1.DiscardNull = true;
                byte[] receiveBuffer = new byte[bytesToRead];                
                //MessageBox.Show(serialPort1.BytesToRead.ToString());                
                serialPort1.Read(receiveBuffer, 0, bytesToRead);
                //string ascii = System.Text.Encoding.ASCII.GetString(receiveBuffer);
                //byte[] hex = System.Text.Encoding.Default.GetBytes(ascii);
                //StringBuilder str = new StringBuilder();
                //List<String> list = new List<string>();
                /*if (hex.Length > 4)
                {
                    list.Add(String.Format("{0:x2} ", hex[2]));
                    list.Add(String.Format("{0:x2} ", hex[3]));
                    list.Add(String.Format("{0:x2} ", hex[4]));
                    list.Reverse();
                    foreach (String s in list)
                        str.Append(s.Trim());
                    writeCell(str.ToString());
                }*/
                string[] values;
                switch (receiveBuffer.Length){
                    case 3:
                        values = receiveBuffer.Select(v => String.Format("{0:x2} ", v).Trim()).Reverse().ToArray();
                        //writeCell(hex.Select(v => String.Format("{0:x2} ", v)).ToArray().Reverse().First());
                        writeCell(String.Format("{0}",String.Join(",",values[0])));
                        
                        break;
                    case 4:
                        values = receiveBuffer.Select(v => String.Format("{0:x2} ", v).Trim()).Reverse().ToArray();
                        writeCell(String.Format("{0}{1}", values[0], values[1]));
                        break;
                default:
                        writeCell("0");
                        break;
                
               }
                //writeCell(serialPort1.ReadExisting());
                //writeCell(bytesToRead.ToString());

                //row1++; 
                #endregion

            }
            catch (Exception ex)
            {
                
               
            }
        }      

        private void writeCell(String val)
        {
            //This is the Excel file that is already open so we don't need to re-open it
            Microsoft.Office.Interop.Excel.Application excelObj;
            //Make sure it is active            
            excelObj = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
            //Get the Active workbook
            Microsoft.Office.Interop.Excel.Workbook wb;
            wb = excelObj.ActiveWorkbook;
            //To get the top sheet (e.g. Sheet1) or the Active Sheet use this syntax
            //Microsoft.Office.Interop.Excel.Worksheet sheet = (Microsoft.Office.Interop.Excel.Worksheet)this.ActiveSheet;

            //Get a handle on all the worksheets in the Workbook
            Microsoft.Office.Interop.Excel.Sheets sheets = (Microsoft.Office.Interop.Excel.Sheets)wb.Worksheets;
            //Get a specific sheet in the Workbook
            Microsoft.Office.Interop.Excel.Worksheet sheet = (Microsoft.Office.Interop.Excel.Worksheet)sheets.get_Item(1);
            //To get a cell or group of cells, you can use the following synatx
            //Microsoft.Office.Interop.Excel.Range afield = (Microsoft.Office.Interop.Excel.Range)excelObj.ActiveCell; //.get_Range(String.Format("A{0}", row1), System.Reflection.Missing.Value);
            Microsoft.Office.Interop.Excel.Worksheet xlSheet = wb.ActiveSheet;

            Microsoft.Office.Interop.Excel.Range afield = xlSheet.get_Range(String.Format("A{0}", 1), System.Reflection.Missing.Value);
            //Set the value of the A1 cell equal to "Hello World" plus the value in the name field in the dialogbox
            afield.set_Value(System.Reflection.Missing.Value, val);
        }

        
        public List<string> GetAllPorts()
        {
            List<String> allPorts = new List<String>();
            foreach (String portName in System.IO.Ports.SerialPort.GetPortNames())
            {
                allPorts.Add(portName);
            }
            return allPorts;
        }
        private void cbPorts_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
            if (!this.cbPort1.SelectedItem.Label.Equals(""))
            {
                if (!ValidatePortSelected(this.cbPort1.SelectedItem.Label, this.cbPort1.Id))
                {

                    foreach (RibbonControl ctrl in this.group1.Items)
                    {
                        if (ctrl != btnClosePort)
                        ctrl.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("This port is selected ");
                }
            }
        }

        

        private void btnClosePort_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {

                if (serialPort1.IsOpen)
                {
                    serialPort1.Close();
                    this.btnReadSerialPort.Enabled = true;
                    this.btnClosePort.Enabled = false;
                    foreach (RibbonControl ctrl in this.group1.Items)
                    {
                        if(ctrl != cbPort1)
                        ctrl.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
       
    }
}
